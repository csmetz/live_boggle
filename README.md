# Letter Square

Letter Square is a boggle clone made with Phoenix LiveView and Surface.

It allows you to play Boggle with other players in real time.

Currently, French and English are supported, but it should be easy to
add other languages.

## Start the application

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Run the tests

```elixir
mix test
```
