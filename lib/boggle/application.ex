defmodule Boggle.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      {Registry, keys: :unique, name: Boggle.Registry},
      BoggleWeb.Telemetry,
      {Phoenix.PubSub, name: Boggle.PubSub},
      Players,
      Supervisor.child_spec({GameServer, locale: "fr"}, id: :game_server_fr),
      Supervisor.child_spec({GameServer, locale: "en"}, id: :game_server_en),
      BoggleWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Boggle.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    BoggleWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
