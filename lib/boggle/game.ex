defmodule Game do
  defstruct grid: nil, duration: 180, words: []

  @type t() :: %__MODULE__{
          grid: Grid.t(),
          duration: pos_integer(),
          words: [String.t()]
        }

  def score_for_word(word) do
    case String.length(word) do
      l when l <= 2 -> 0
      3 -> 1
      4 -> 1
      5 -> 2
      6 -> 3
      7 -> 5
      l when l >= 8 -> 11
    end
  end

  def score_for_words(words) do
    words
    |> Enum.map(&score_for_word/1)
    |> Enum.sum()
  end

  @spec new(String.t()) :: Game.t()
  def new(locale) do
    grid = Grid.random(4, 4, locale)
    words = WordSearch.list_all(grid, Language.dictionary(locale))

    %Game{
      grid: grid,
      duration: 180,
      words: words
    }
  end

  @spec default :: Game.t()
  def default do
    grid = Grid.new(['????', '????', '????', '????'])

    %Game{
      grid: grid,
      duration: 180,
      words: []
    }
  end

  @spec max_score(Game.t()) :: number
  def max_score(%Game{words: words} = _game) do
    score_for_words(words)
  end
end
