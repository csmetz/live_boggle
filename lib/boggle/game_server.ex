defmodule GameServer do
  use GenServer

  @results_duration_seconds 75

  defmodule State do
    defstruct game: nil,
              remaining_seconds: 180,
              phase: :play,
              scores: %{},
              end_summary: %{},
              locale: "fr",
              topic: "game_updates",
              chat_messages: []

    @type t() :: %__MODULE__{
            game: Game.t(),
            remaining_seconds: number(),
            phase: :play | :show_results,
            scores: map,
            end_summary: map,
            locale: String.t(),
            topic: String.t(),
            chat_messages: [map]
          }
  end

  def genserver_name(locale), do: "GameServer_" <> locale

  def for(locale) do
    case Registry.lookup(Boggle.Registry, genserver_name(locale)) do
      [{pid, _}] -> pid
      _ -> nil
    end
  end

  def start_link(opts) do
    locale = opts |> Keyword.get(:locale)

    GenServer.start_link(
      __MODULE__,
      opts,
      name: {:via, Registry, {Boggle.Registry, genserver_name(locale)}}
    )
  end

  def init(opts) do
    locale = opts |> Keyword.get(:locale)

    tick()

    {:ok, new_state(locale)}
  end

  defp pubsub_topic(locale), do: "game_updates_" <> locale

  defp new_state(locale) do
    game = Game.new(locale)

    %State{
      game: game,
      remaining_seconds: game.duration,
      phase: :play,
      scores: %{},
      locale: locale,
      topic: pubsub_topic(locale)
    }
  end

  def state(pid) do
    pid |> GenServer.call(:get_state)
  end

  def submit_word(pid, player, word) do
    pid |> GenServer.cast({:submit_word, player, word})
  end

  def register_user(pid, player) do
    pid |> GenServer.cast({:register_user, player})
  end

  def chat_message(pid, user, text) do
    pid |> GenServer.cast({:chat_message, user, text})
  end

  def tick() do
    Process.send_after(self(), :tick, 1_000)
  end

  def handle_info(:tick, state) do
    remaining = state.remaining_seconds - 1
    Phoenix.PubSub.broadcast(Boggle.PubSub, state.topic, {:tick, remaining})
    tick()

    # Refresh the number of players less frequently to avoid
    # sending too many events for nothing
    if rem(remaining, 5) == 4 do
      Phoenix.PubSub.broadcast(
        Boggle.PubSub,
        state.topic,
        {:number_playing, state.scores |> Enum.count()}
      )
    end

    cond do
      remaining > 0 ->
        {:noreply, %{state | remaining_seconds: remaining}}

      state.phase == :play ->
        summary = results_summary(state)

        new_state = %{
          state
          | phase: :show_results,
            remaining_seconds: @results_duration_seconds,
            end_summary: summary
        }

        Phoenix.PubSub.broadcast(Boggle.PubSub, state.topic, {:end, new_state})

        {:noreply, new_state}

      state.phase == :show_results ->
        new_state = new_state(state.locale)
        Phoenix.PubSub.broadcast(Boggle.PubSub, state.topic, {:reset, new_state})
        {:noreply, new_state}
    end
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  def handle_cast({:register_user, player}, state) do
    new_scores = state.scores |> Map.update(player, [], & &1)
    {:noreply, %{state | scores: new_scores}}
  end

  def handle_cast({:submit_word, player, word}, state) do
    new_scores =
      state.scores
      |> Map.update(player, [word], fn words -> [word | words] end)

    {:noreply, %{state | scores: new_scores}}
  end

  def handle_cast({:chat_message, user, text}, state) do
    message = %{user: user, text: text, time: DateTime.utc_now()}

    Phoenix.PubSub.broadcast(Boggle.PubSub, state.topic, {:chat_update, message})

    {:noreply, %{state | chat_messages: [message | state.chat_messages]}}
  end

  defp results_summary(%State{game: _game, scores: scores}) do
    %{
      ranking:
        scores
        |> Enum.map(fn {user, words} -> {user, Game.score_for_words(words)} end)
        |> Enum.sort_by(&elem(&1, 1), :desc)
    }
  end
end
