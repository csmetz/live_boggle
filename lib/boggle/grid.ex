defmodule Grid do
  defstruct width: 4, height: 4, letters: nil

  @type t() :: %__MODULE__{
          width: non_neg_integer(),
          height: non_neg_integer(),
          letters: tuple()
        }

  @spec new([[char]]) :: Grid.t()
  def new(letters) when is_list(letters) do
    if length(letters) == 0 or length(letters |> hd()) == 0 do
      raise "Error: the grid width or height cannot be zero."
    end

    %Grid{
      width: length(letters |> hd()),
      height: length(letters),
      letters: letters |> Enum.map(&List.to_tuple/1) |> List.to_tuple()
    }
  end

  # We use a quite involved generation method to increase the probability
  # of getting a lot of words in the grid, and longer words.
  # We do this by following paths in the grid (always reaching a position
  # for which no letter was chosen yet). The probability of choosing a letter
  # at one position depends on the previous letter in the path, and is based
  # on the bigrams frequency, computed from the dictionary.
  @spec random(integer, integer, binary) :: Grid.t()
  def random(width, height, locale \\ "fr") do
    letters = for _h <- 1..height, do: for(_w <- 1..width, do: nil)

    grid = Grid.new(letters)
    bigrams = Language.bigrams(locale)

    start_x = Enum.random(0..(width - 1))
    start_y = Enum.random(0..(height - 1))
    start_distribution = bigrams[Enum.random(?a..?z)]

    fill_from_bigrams(grid, bigrams, {start_x, start_y}, start_distribution)
  end

  defp fill_from_bigrams(grid, bigrams, {x, y} = _current, current_weights) do
    letter = Utils.Random.weighted_choice(current_weights)
    next_grid = grid |> set_letter(x, y, letter)

    next = random_unset_neighbour(grid, {x, y}) || next_unset(grid)

    if next do
      fill_from_bigrams(next_grid, bigrams, next, bigrams[letter])
    else
      next_grid
    end
  end

  defp next_unset(grid) do
    for(
      x <- 0..(grid.width - 1),
      y <- 0..(grid.height - 1),
      Grid.at(grid, x, y) == nil,
      do: {x, y}
    )
    |> Enum.at(0)
  end

  defp random_unset_neighbour(grid, {x, y}) do
    unset_neighbours = for {{i, j}, nil} <- Grid.neighbours(grid, x, y), do: {i, j}

    if unset_neighbours == [] do
      nil
    else
      unset_neighbours |> Enum.random()
    end
  end

  defp set_letter(%Grid{} = grid, x, y, value) do
    row = grid.letters |> elem(y)
    updated_row = row |> put_elem(x, value)
    updated_letters = grid.letters |> put_elem(y, updated_row)
    %{grid | letters: updated_letters}
  end

  @spec at(Grid.t(), non_neg_integer, non_neg_integer) :: char | nil
  def at(%Grid{} = grid, x, y) do
    grid.letters |> elem(y) |> elem(x)
  end

  @spec width(Grid.t()) :: non_neg_integer
  def width(%Grid{} = grid) do
    grid.width
  end

  @spec height(Grid.t()) :: non_neg_integer
  def height(%Grid{} = grid) do
    grid.height
  end

  def neighbours(%Grid{width: width, height: height} = grid, x, y) do
    for i <- (x - 1)..(x + 1),
        i in 0..(width - 1),
        j <- (y - 1)..(y + 1),
        j in 0..(height - 1),
        {i, j} != {x, y},
        do: {{i, j}, grid |> at(i, j)}
  end

  defimpl Inspect, for: __MODULE__ do
    def inspect(grid, _opts) do
      for row <- Tuple.to_list(grid.letters),
          letter <- Tuple.to_list(row) do
        letter
      end
      |> Enum.chunk_every(grid.width)
      |> Enum.map(&to_string/1)
      |> Enum.map(&String.upcase/1)
      |> Enum.join("\n")
    end
  end
end
