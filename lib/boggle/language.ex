defmodule Language do
  defp file(lang, filename), do: :boggle |> Application.app_dir("priv/lang/#{lang}/#{filename}")

  @spec dictionary(binary) :: Stream.t(binary)
  def dictionary(lang) do
    lang
    |> file("dictionary.txt")
    |> File.stream!()
    |> Stream.map(&String.trim/1)
    |> Stream.filter(&(String.length(&1) >= 3))
    |> Stream.map(&String.downcase/1)
  end

  @spec bigrams(binary) :: map
  def bigrams(lang) do
    lang
    |> file("bigrams.txt")
    |> File.read!()
    |> Utils.Frequency.read_bigrams()
  end
end
