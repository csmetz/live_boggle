defmodule Players do
  @moduledoc """
  This GenServer serves as a very basic in-memory DB to store
  usernames and sessions. The DB won't survive a restart of the
  application or the GenServer but is more than enough for its purpose.

  Note: an Agent might do the job as well
  """
  use GenServer
  require Logger

  defmodule Player do
    defstruct cookie: nil, username: 180, last_seen: DateTime.utc_now()

    @type t() :: %__MODULE__{
            cookie: String.t(),
            username: String.t(),
            last_seen: DateTime.t()
          }
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_state) do
    {:ok, %{}}
  end

  def add(cookie, username) do
    __MODULE__ |> GenServer.call({:add, cookie, username})
  end

  def get(cookie) do
    __MODULE__ |> GenServer.call({:get, cookie})
  end

  def handle_call({:add, cookie, username}, _from, players) do
    cond do
      String.length(username) not in 1..20 ->
        {:reply, :bad_length, players}

      players |> username_taken?(username) ->
        {:reply, :already_exists, players}

      true ->
        Logger.info("#{username} has entered the Letter Square!")
        {:reply, :ok, players |> Map.put(cookie, %Player{cookie: cookie, username: username})}
    end
  end

  def handle_call({:get, cookie}, _from, players) do
    {:reply, players[cookie], players}
  end

  defp username_taken?(players, username) do
    players
    |> Enum.any?(fn {_, player} ->
      String.downcase(player.username) == String.downcase(username)
    end)
  end
end
