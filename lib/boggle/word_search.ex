defmodule WordSearch do
  @spec list_all(Grid.t(), [binary]) :: [binary]
  @spec list_all(Grid.t(), Stream.t(binary)) :: [binary]
  def list_all(%Grid{} = grid, dictionary) when is_list(dictionary) do
    trie = get_trie(dictionary)

    grid
    |> find(trie)
    |> Enum.uniq()
    |> Enum.sort_by(&String.length/1, :desc)
  end

  # This alternative using a stream is necessary to reduce the memory
  # usage. Loading the entire dictionary and turning it into a trie
  # can exceed the 500MB available on the free Gigalixir instance.
  # Besides, it is not really slower.
  def list_all(%Grid{} = grid, dict_stream) do
    dict_stream
    |> Stream.chunk_every(5_000)
    |> Stream.map(fn subdictionary ->
      list_all(grid, subdictionary)
    end)
    |> Enum.into([])
    |> List.flatten()
    |> Enum.sort_by(&String.length/1, :desc)
  end

  defp get_trie(dictionary) do
    Retrieval.new(dictionary)
  end

  defp find(%Grid{} = grid, trie) do
    initial_paths =
      for i <- 0..(Grid.width(grid) - 1),
          j <- 0..(Grid.height(grid) - 1) do
        {{i, j}, <<grid |> Grid.at(i, j)>>, []}
      end

    find(grid, trie, initial_paths, [])
  end

  defp find(%Grid{} = _grid, _trie, [], found_words) do
    found_words
  end

  defp find(%Grid{} = grid, trie, [{{x, y}, prefix, visited} | next_paths] = _paths, found_words) do
    found_words_updated =
      if trie |> Retrieval.contains?(prefix) do
        [prefix | found_words]
      else
        found_words
      end

    new_paths =
      for {{i, j}, letter} <- Grid.neighbours(grid, x, y),
          {i, j} not in visited,
          new_prefix = prefix <> <<letter>>,
          trie |> Retrieval.prefix(new_prefix) |> length > 0 do
        {{i, j}, new_prefix, [{x, y} | visited]}
      end

    next_paths = new_paths |> Enum.reduce(next_paths, &[&1 | &2])

    find(grid, trie, next_paths, found_words_updated)
  end

  def find_path_for(%Grid{} = grid, word) do
    initial_paths =
      for i <- 0..(Grid.width(grid) - 1),
          j <- 0..(Grid.height(grid) - 1) do
        {{i, j}, word, []}
      end

    _find_path_for(grid, initial_paths)
  end

  defp _find_path_for(_grid, []) do
    []
  end

  defp _find_path_for(_grid, [{_, <<>>, visited} | _]) do
    visited |> Enum.reverse()
  end

  defp _find_path_for(%Grid{} = grid, [{{x, y}, <<letter, rest::binary>>, visited} | next_paths]) do
    cond do
      grid |> Grid.at(x, y) != letter ->
        _find_path_for(grid, next_paths)

      rest == "" ->
        [{x, y} | visited] |> Enum.reverse()

      true ->
        new_paths =
          for {{i, j}, _letter} <- Grid.neighbours(grid, x, y),
              {i, j} not in visited do
            {{i, j}, rest, [{x, y} | visited]}
          end

        _find_path_for(grid, next_paths ++ new_paths)
    end
  end
end
