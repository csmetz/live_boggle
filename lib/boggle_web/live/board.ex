defmodule BoggleWeb.ShowGrid do
  use Surface.Component

  prop grid, :map, default: Grid.new(['????', '????', '????', '????'])
  prop highlighted, :list, default: [{1, 1}]

  def render(assigns) do
    ~H"""
    <div class="grid">
      <div :for={{ j <- 1..Grid.height(@grid), i <- 1..Grid.width(@grid)}} class={{ "letter", highlighted: {i-1, j-1} in @highlighted }}>
        <!-- We use an SVG to make the letter change size with the die size, which was not
        really possible with CSS. -->
        <svg
          width="100"
          height="100"
          viewBox="0 0 100 100"
          preserveAspectRatio="xMidYMid meet"
          xmlns="http://www.w3.org/2000/svg"
          xmlns:xlink="http://www.w3.org/1999/xlink">
          <text
            x="50"
            y={{ if letter(@grid, i, j) == "Q", do: "80", else: "90" }}
            text-anchor="middle"
            font-size={{ if letter(@grid, i, j) == "Q", do: "90", else: "100" }}
            fill="black">
            {{ @grid |> letter(i, j) }}
          </text>
        </svg>
      </div>
    </div>
    """
  end

  defp letter(grid, i, j) do
    <<grid |> Grid.at(i - 1, j - 1)>> |> String.upcase()
  end
end

defmodule BoggleWeb.SubmitWord do
  use Surface.LiveComponent
  require Logger
  import BoggleWeb.Gettext

  data word, :string, default: ""

  prop disabled, :boolean, default: false

  def render(assigns) do
    ~H"""
    <form :on-submit="submit_word" :on-change="change" class="submit">
      <input
        type="text"
        name="word"
        value={{ @word }}
        pattern="[a-zA-Z]+"
        disabled={{ @disabled }}
        autocomplete="off"
        />
      <button type="submit" disabled={{ @disabled }}>
        {{ gettext "Submit" }}
      </button>
    </form>
    """
  end

  def handle_event("change", %{"word" => word}, socket) do
    filtered_word =
      word
      |> String.to_charlist()
      |> Enum.filter(fn char -> char in ?a..?z or char in ?A..?Z end)
      |> List.to_string()

    Process.send(self(), {:word_update, word}, [:noconnect])

    {:noreply, assign(socket, :word, filtered_word)}
  end

  def handle_event("submit_word", %{"word" => word}, socket) do
    word = String.downcase(word)

    if word != "" do
      Process.send(self(), {:submit_word, word}, [:noconnect])
    end

    {:noreply, assign(socket, :word, "")}
  end

  def handle_event(type, params, socket) do
    Logger.warn("Unhandled event #{type} with params: #{inspect(params)}")
    {:noreply, socket}
  end
end
