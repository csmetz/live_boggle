defmodule BoggleWeb.Chat do
  use Surface.LiveComponent
  import BoggleWeb.Gettext

  prop messages, :list, default: []
  data max_message_length, :number, default: 500
  data current_input, :string, default: ""

  def render(assigns) do
    ~H"""
    <div class="chat-wrapper">
      <div class="chat">
        <h2> Chat </h2>
        <div class="messages">
          <div class="message" :for={{ message <- @messages }}>
            <span class="message-user"> {{ message.user }} </span>
            <span class="message-time"> {{ displayed_time(message.time) }} </span>
            <span class="message-content"> {{ message.text }} </span>
          </div>
        </div>

        <form :on-submit="submit_message" :on-change="change" class="submit-chat">
          <input
            type="text"
            name="message"
            value={{ @current_input }}
            maxlength={{ @max_message_length }} />
          <button type="submit"> {{ gettext "Send" }} </button>
        </form>
      </div>
    </div>
    """
  end

  defp displayed_time(%DateTime{} = date) do
    date
    |> DateTime.to_time()
    |> Time.truncate(:second)
  end

  def handle_event("submit_message", %{"message" => message}, socket) do
    if String.trim(message) != "" do
      message = message |> String.slice(0..socket.assigns.max_message_length)
      Process.send(self(), {:submit_message, message}, [:noconnect])
    end

    {:noreply, assign(socket, :current_input, "")}
  end

  def handle_event("change", %{"message" => message}, socket) do
    {:noreply, assign(socket, :current_input, message)}
  end
end
