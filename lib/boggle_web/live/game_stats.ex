defmodule BoggleWeb.Stat do
  use Surface.Component

  prop name, :string, default: ""
  prop full, :boolean, default: false
  slot default

  def render(assigns) do
    ~H"""
    <div class={{ "stat", full: @full }}>
      <p class="stat-name"> {{ @name }} </p>
      <p class="stat-value"> <slot /> </p>
    </div>
    """
  end
end

defmodule BoggleWeb.Timer do
  use Surface.Component
  alias BoggleWeb.Stat
  import BoggleWeb.Gettext

  prop seconds, :integer

  def render(assigns) do
    ~H"""
    <Stat name={{ gettext "Time left" }} full>
      <span class={{ blinking: @seconds <= 10 }}>
        {{ format_time(@seconds) }}
      </span>
    </Stat>
    """
  end

  def format_time(seconds) do
    minutes = div(seconds, 60)

    seconds_padded =
      seconds
      |> rem(60)
      |> Integer.to_string()
      |> String.pad_leading(2, "0")

    "#{minutes}:#{seconds_padded}"
  end
end

defmodule BoggleWeb.GameStats do
  use Surface.LiveComponent
  alias BoggleWeb.{Timer, Stat}
  import BoggleWeb.Gettext

  prop user, :string
  prop seconds, :integer
  prop game, :map
  prop words_found, :list, default: []
  prop game_finished, :boolean
  prop end_summary, :map, default: %{}

  def render(assigns) do
    ~H"""
    <div class="game_stats">
      <div :if={{ not @game_finished }}>
        <Timer seconds={{ @seconds }} />
        <Stat name={{ gettext "Score" }}>
          {{ Game.score_for_words(@words_found) }}

          <!-- Temporary feedback to show how much points were scored.
          A CSS animation is used to make it disappear quickly. Generating
          a unique id everytime is important to make sure a new element is
          generated and the animation is triggered again everytime. -->
          <span
            :if={{ last_word(@words_found) != "" }}
            id={{ "word-" <> last_word(@words_found) }}
            class="mark-points">
            +{{ Game.score_for_word(last_word(@words_found)) }}
          </span>
        </Stat>
        <Stat name={{ gettext "Found" }}> {{ length(@words_found) }} </Stat>
      </div>

      <div :if={{ @game_finished }}>
        <p class="full">
          {{ gettext "Time's up!" }} <br>
          {{ gettext "Next game in %{seconds} seconds...", seconds: @seconds }} <br>
        </p>

        <Stat name={{ gettext "Score" }}> {{ Game.score_for_words(@words_found) }} / {{ Game.max_score(@game) }} </Stat>
        <Stat name={{ gettext "Found"}}> {{ length(@words_found) }} / {{ length(@game.words) }} </Stat>

        <Stat :if={{ @words_found |> length() > 0 }} name={{gettext "Ranking"}} full>
          {{ own_ranking(@user, @end_summary) }} / {{ length(@end_summary.ranking) }}
          <span :if={{ own_ranking(@user, @end_summary) == 1 }}> 🏆 </span>
        </Stat>

        <div class="ranking">
          <table>
            <thead>
              <tr>
                <th> {{ gettext "Rank" }} </th>
                <th> {{ gettext "Name" }} </th>
                <th> {{ gettext "Score" }} </th>
              </tr>
            </thead>
            <tbody>
              <tr :for.with_index={{ {{player, score}, rank} <- @end_summary.ranking }} class={{ selected: player == @user }}>
                <td> {{ rank + 1 }} </td>
                <td> {{ player }} </td>
                <td> {{ score }} </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    """
  end

  defp last_word([]), do: ""
  defp last_word([word | _]), do: word

  def own_ranking(user, end_summary) do
    Enum.find_index(end_summary.ranking, &(elem(&1, 0) == user)) + 1
  end
end
