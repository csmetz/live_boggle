defmodule BoggleWeb.Header do
  use Surface.LiveComponent
  import BoggleWeb.Gettext
  @default_locales ["fr", "en"]

  data locales, :list, default: @default_locales
  data show_rules, :boolean, default: false
  prop current, :string

  def render(assigns) do
    ~H"""
    <header>
      <div class="logo">
        <h1>
          <a href="/play">
            <span class="first-letter">L</span>etter <span class="first-letter">S</span>quare
          </a>
        </h1>
      </div>
      <div class="language-select">
        <span :for.with_index={{ {locale, index} <- @locales }}>
          <span :if={{ index > 0}}> · </span>
          <a href={{ "/#{locale}/play" }} class={{ selected: @current == locale }}>
            {{ locale }}
          </a>
        </span>
      </div>

      <!-- TODO: extract to a modal component... -->
      <div class="header-rules" :on-click="show_rules">
        <a href="#">📝 {{ gettext "Rules of the game" }}</a>
      </div>
      <div :if={{ @show_rules }} class="rules">
        <div class="content">
          <h1> {{ gettext "Rules of the game" }} </h1>
          <ul>
            <li> {{ gettext "Find as many words as possible in the grid in 3 minutes!" }} </li>
            <li> {{ gettext "Words should follow a path of adjacent letters in the grid, and never pass twice by the same position for the same word." }} </li>
            <li> {{ gettext "Any English word, including conjugated verbs or words in plural form are accepted." }} </li>
          </ul>
          <button :on-click="show_rules"> {{ gettext "Understood!" }} </button>
        </div>
      </div>
    </header>
    """
  end

  def handle_event("show_rules", _, socket) do
    {:noreply, assign(socket, show_rules: not socket.assigns.show_rules)}
  end
end
