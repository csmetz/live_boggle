defmodule BoggleWeb.LoginLive do
  use Surface.LiveView
  alias Players.Player
  alias BoggleWeb.Header
  import BoggleWeb.Gettext

  data cookie, :string, default: ""
  data message, :string, default: ""
  data locale, :string, default: "fr"

  def handle_params(%{"locale" => locale} = _params, _uri, socket) do
    Gettext.put_locale(BoggleWeb.Gettext, locale)
    {:noreply, assign(socket, locale: locale)}
  end

  def mount(params, session, socket) do
    case Players.get(session["cookie"]) do
      nil ->
        {:ok, assign(socket, cookie: session["cookie"])}

      %Player{} ->
        {:ok, push_redirect(socket, to: "/#{params["locale"]}/play")}
    end
  end

  def render(assigns) do
    ~H"""
    <Header id="header" current={{ @locale }} />
    <div class="login">
      <h2> {{ gettext "Choose your name:" }} </h2>
      <form :on-submit="set_username" class="username">
        <input
          type="text"
          name="username" />
        <button type="submit"> {{ gettext "Play!" }} </button>
      </form>
      {{ @message }}
    </div>
    """
  end

  def handle_event("set_username", %{"username" => username}, socket) do
    # GameServer.update_username(socket.assigns.user, username)
    case Players.add(socket.assigns.cookie, username) do
      :bad_length ->
        {:noreply,
         assign(socket, message: gettext("The username should have between 1 and 20 characters."))}

      :already_exists ->
        {:noreply, assign(socket, message: gettext("This username is already taken!"))}

      :ok ->
        {:noreply, push_redirect(socket, to: "/play")}
    end
  end
end
