defmodule BoggleWeb.PlayLive do
  use Surface.LiveView
  alias BoggleWeb.{ShowGrid, SubmitWord, WordsFound, GameStats, Header, Chat}
  alias Players.Player
  import BoggleWeb.Gettext

  data user, :string, default: ""
  data game, :map, default: Game.default()
  data words_found, :list, default: []
  data remaining_seconds, :integer, default: 180
  data message, :string, default: ""
  data word_input, :string, default: ""
  data end_summary, :map, default: %{}
  data phase, :atom, default: :play
  data number_playing, :number, default: 1
  data game_server, :any
  data locale, :string, default: "fr"
  data chat_messages, :list, default: []

  def handle_params(%{"locale" => locale} = _params, _uri, socket) do
    Gettext.put_locale(BoggleWeb.Gettext, locale)
    game_server = GameServer.for(locale)
    {:noreply, assign(socket, game_server: game_server, locale: locale)}
  end

  def mount(params, session, socket) do
    Process.send(self(), :start_game, [:noconnect])

    case Players.get(session["cookie"]) do
      nil ->
        {:ok, push_redirect(socket, to: "/#{params["locale"]}/login")}

      %Player{} = player ->
        {:ok, assign(socket, user: player.username)}
    end
  end

  def render(assigns) do
    ~H"""
    <Header id="header" current={{ @locale }} />
    <div class={{ "boggle", "results-phase": game_finished?(assigns) }}>
      <div :if={{ not game_finished?(assigns) }} class="board">
        <ShowGrid
          grid={{ @game.grid }}
          highlighted={{ WordSearch.find_path_for(@game.grid, @word_input) }} />
        <SubmitWord id="submit" disabled={{ game_finished?(assigns) }} />
        <p> {{ @message }} </p>
      </div>
      <Chat id="chat" :if={{ game_finished?(assigns) }} messages={{ @chat_messages }} />
      <p
        :if={{ game_finished?(assigns) and @remaining_seconds <= 5 }}
        class="countdown"
        id={{ "countdown-#{@remaining_seconds}" }}>
        {{ if @remaining_seconds == 0, do: "Go!", else: @remaining_seconds }}
      </p>

      <div class="words-info">
        <div
          :if={{ game_finished?(assigns) }}
          class={{ "board", absent: game_finished?(assigns) and @remaining_seconds <= 5 }}>
          <ShowGrid
            grid={{ @game.grid }}
            highlighted={{ WordSearch.find_path_for(@game.grid, @word_input) }} />
        </div>
        <WordsFound
          id="words"
          words={{ @words_found }}
          all_words={{ @game.words }}
          show_all={{ game_finished?(assigns) }}
          />
      </div>

      <GameStats
        id="game-stats"
        user={{ @user }}
        seconds={{ @remaining_seconds }}
        game={{ @game }}
        words_found={{ @words_found }}
        end_summary={{ @end_summary }}
        game_finished={{ game_finished?(assigns) }}
        />

      <p class="players">
        <span class="username"> {{ @user }} </span> <br>
        <span class="others-online" title={{ gettext "playing with you now" }}>
          👥 {{ @number_playing }}
        </span>
      </p>
    </div>
    """
  end

  defp game_finished?(%{phase: :play} = _assigns), do: false
  defp game_finished?(%{phase: :show_results} = _assigns), do: true

  def handle_info({:submit_word, word}, socket) do
    %{assigns: %{game: game, words_found: words_found, user: user, game_server: game_server}} =
      socket

    socket = assign(socket, word_input: "")

    cond do
      word in words_found ->
        {:noreply, assign(socket, message: gettext("Word already found!"))}

      word not in game.words ->
        {:noreply,
         assign(socket, message: gettext("%{word} does not exist", word: String.upcase(word)))}

      not game_finished?(socket.assigns) ->
        game_server |> GameServer.submit_word(user, word)
        {:noreply, assign(socket, words_found: [word | words_found], message: "")}
    end
  end

  def handle_info({:word_update, word}, socket) do
    {:noreply, assign(socket, word_input: word)}
  end

  def handle_info({:highlight, word}, socket) do
    {:noreply, assign(socket, word_input: word)}
  end

  def handle_info({:tick, seconds}, socket) do
    {:noreply, assign(socket, :remaining_seconds, seconds)}
  end

  def handle_info({:number_playing, number}, socket) do
    {:noreply, assign(socket, :number_playing, number)}
  end

  def handle_info(:start_game, socket) do
    %{user: user, game_server: game_server} = socket.assigns

    state = game_server |> GameServer.state()
    Phoenix.PubSub.subscribe(Boggle.PubSub, state.topic)

    if state.phase == :play do
      game_server
      |> GameServer.register_user(user)
    end

    {
      :noreply,
      assign(
        socket,
        game: state.game,
        remaining_seconds: state.remaining_seconds,
        words_found: state.scores[socket.assigns.user] || [],
        phase: state.phase,
        end_summary: state.end_summary,
        chat_messages: state.chat_messages
      )
    }
  end

  def handle_info({:end, state}, socket) do
    {:noreply,
     assign(socket,
       phase: state.phase,
       end_summary: state.end_summary,
       remaining_seconds: state.remaining_seconds
     )}
  end

  def handle_info({:reset, state}, socket) do
    socket.assigns.game_server
    |> GameServer.register_user(socket.assigns.user)

    {:noreply,
     assign(socket,
       game: state.game,
       remaining_seconds: state.remaining_seconds,
       words_found: [],
       end_summary: %{},
       phase: state.phase,
       word_input: "",
       chat_messages: []
     )}
  end

  def handle_info({:submit_message, message_text}, socket) do
    socket.assigns.game_server
    |> GameServer.chat_message(socket.assigns.user, message_text)

    {:noreply, socket}
  end

  def handle_info({:chat_update, message}, socket) do
    updated_messages = [message | socket.assigns.chat_messages]
    {:noreply, socket |> assign(:chat_messages, updated_messages)}
  end
end
