defmodule BoggleWeb.WordsFound do
  use Surface.LiveComponent
  import BoggleWeb.Gettext

  prop words, :list, default: []
  prop all_words, :list, default: []
  prop show_all, :boolean, default: false

  def render(assigns) do
    ~H"""
    <div class="words_list">
      <section :if={{ not @show_all }}>
        <h2> {{ gettext "Words found (%{count})", count: length(@words) }} </h2>
        <ul :if={{ length(@words) > 0 }}>
          <li
            :for={{ word <- Enum.reverse(@words) }}
            :on-click={{ "click_word_#{word}" }}
            >
            {{ word }}
          </li>
        </ul>
      </section>

      <section :if={{ @show_all }}>
        <h2> {{ gettext "All words (%{count})", count: length(@all_words) }} </h2>
        <ul>
          <li
            :for={{ word <- @all_words }}
            :on-click={{ "click_word_#{word}" }}
            class={{ found: word in @words }}
            >
            {{ word }}
          </li>
        </ul>
      </section>
    </div>
    """
  end

  def handle_event("click_word_" <> word, _assigns, socket) do
    Process.send(self(), {:highlight, word}, [:noconnect])
    {:noreply, socket}
  end
end
