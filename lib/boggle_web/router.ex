defmodule BoggleWeb.Router do
  use BoggleWeb, :router
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {BoggleWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :validate_session

    plug SetLocale,
      gettext: BoggleWeb.Gettext,
      default_locale: "en",
      cookie_key: "project_locale",
      additional_locales: ["fr"]
  end

  pipeline :browser_dev do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {BoggleWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :validate_session
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  def validate_session(conn, _opts) do
    case get_session(conn, :cookie) do
      nil ->
        conn
        |> put_session(:cookie, Ecto.UUID.generate())

      _cookie ->
        conn
    end
  end

  scope "/", BoggleWeb do
    pipe_through :browser

    live "/", PageLive, :index
  end

  scope "/:locale", BoggleWeb do
    pipe_through :browser

    live "/", PageLive, :index

    live "/play", PlayLive, :index
    live "/login", LoginLive, :index
  end

  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser_dev
      live_dashboard "/dashboard"
    end
  end
end
