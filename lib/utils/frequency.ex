defmodule Utils.Frequency do
  def bigram_frequency(dictionary) do
    initial_frequencies =
      for first <- ?a..?z,
          into: %{},
          do: {first, for(second <- ?a..?z, into: %{}, do: {second, 0})}

    dictionary
    |> Stream.flat_map(fn word ->
      word
      |> String.to_charlist()
      |> Enum.chunk_every(2, 1, :discard)
    end)
    |> Enum.reduce(initial_frequencies, fn [a, b], frequencies ->
      frequencies |> update_in([a, b], &(&1 + 1))
    end)
  end

  def print_bigrams(bigrams) do
    for(a <- ?a..?z, b <- ?a..?z, do: <<a, b>> <> " #{bigrams[a][b]}")
    |> Enum.join("\n")
  end

  def read_bigrams(string) do
    initial_frequencies = for first <- ?a..?z, into: %{}, do: {first, %{}}

    string
    |> String.split("\n")
    |> Enum.map(fn <<a, b, " ", weight::binary>> ->
      {a, b, String.to_integer(weight)}
    end)
    |> Enum.reduce(initial_frequencies, fn {a, b, weight}, frequencies ->
      frequencies |> put_in([a, b], weight)
    end)
  end
end
