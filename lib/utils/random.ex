defmodule Utils.Random do
  def weighted_choice(%{} = distribution) do
    total = distribution |> Map.values() |> Enum.sum()
    p = :rand.uniform() * total

    find_choice_weighted(distribution |> Enum.into([]), 0, p)
  end

  defp find_choice_weighted([{value, proba} | rest], total_weight, p) do
    if total_weight + proba > p do
      value
    else
      find_choice_weighted(rest, total_weight + proba, p)
    end
  end
end
