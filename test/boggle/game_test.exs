defmodule GameTest do
  use ExUnit.Case, async: true

  test "returns the correct score for a word" do
    assert Game.score_for_word("u") == 0
    assert Game.score_for_word("le") == 0
    assert Game.score_for_word("bim") == 1
    assert Game.score_for_word("cerf") == 1
    assert Game.score_for_word("monde") == 2
    assert Game.score_for_word("boggle") == 3
    assert Game.score_for_word("serpent") == 5
    assert Game.score_for_word("octogone") == 11
    assert Game.score_for_word("hexadecimal") == 11
    assert Game.score_for_word("anticonstitutionnellement") == 11
  end

  test "can return a default game" do
    game = Game.new("fr")

    assert %Grid{} = game.grid
    assert game.duration == 180
  end

  test "can return score for a list of words" do
    assert Game.score_for_words(["plip", "badaboom", "toc", "plouf"]) == 15
    assert Game.score_for_words(["cette", "fonction", "est", "dingue"]) == 17
  end

  test "can return max score for the game" do
    assert Game.max_score(%Game{words: ["plip", "badaboom", "toc", "plouf"]}) == 15
  end
end
