defmodule GridTest do
  use ExUnit.Case, async: true

  defp list_equals?(list1, list2) do
    list1 -- list2 == [] and list2 -- list1 == []
  end

  test "can create a new Grid from a list of list" do
    assert %Grid{} =
             Grid.new([
               [?a, ?b, ?c, ?d],
               [?a, ?b, ?c, ?d],
               [?a, ?b, ?c, ?d],
               [?a, ?b, ?c, ?d]
             ])
  end

  test "can get dimensions of a Grid" do
    grid =
      Grid.new([
        [?a, ?b, ?c],
        [?a, ?b, ?c],
        [?a, ?b, ?c],
        [?a, ?b, ?c]
      ])

    assert grid |> Grid.width() == 3
    assert grid |> Grid.height() == 4
  end

  test "can get character at a specific position" do
    grid =
      Grid.new([
        [?a, ?b, ?c],
        [?d, ?e, ?f],
        [?g, ?h, ?i]
      ])

    assert grid |> Grid.at(0, 0) == ?a
    assert grid |> Grid.at(0, 1) == ?d
    assert grid |> Grid.at(1, 1) == ?e
    assert grid |> Grid.at(2, 1) == ?f
  end

  test "can get neighbours of a position" do
    grid =
      Grid.new([
        [?a, ?b, ?c, ?d],
        [?e, ?f, ?g, ?h],
        [?i, ?j, ?k, ?l],
        [?m, ?n, ?o, ?p]
      ])

    assert grid
           |> Grid.neighbours(0, 0)
           |> list_equals?([{{0, 1}, ?e}, {{1, 0}, ?b}, {{1, 1}, ?f}])

    assert grid
           |> Grid.neighbours(0, 2)
           |> list_equals?([{{0, 1}, ?e}, {{1, 1}, ?f}, {{1, 2}, ?j}, {{0, 3}, ?m}, {{1, 3}, ?n}])

    assert grid
           |> Grid.neighbours(2, 2)
           |> list_equals?([
             {{1, 1}, ?f},
             {{1, 2}, ?j},
             {{1, 3}, ?n},
             {{2, 1}, ?g},
             {{2, 3}, ?o},
             {{3, 1}, ?h},
             {{3, 2}, ?l},
             {{3, 3}, ?p}
           ])
  end

  test "can generate better random grid" do
    grid = Grid.random(4, 4)
    assert grid |> Grid.width() == 4
    assert grid |> Grid.height() == 4
  end
end
