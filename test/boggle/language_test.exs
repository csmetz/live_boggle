defmodule LanguageTest do
  use ExUnit.Case

  test "can load French dictionary" do
    assert Language.dictionary("fr") |> Enum.count() == 386_184
  end

  test "can load English dictionary" do
    assert Language.dictionary("en") |> Enum.count() == 369_650
  end
end
