defmodule WordSearchTest do
  use ExUnit.Case, async: true

  test "can list all words in a grid" do
    grid =
      Grid.new([
        'igre',
        'resa',
        'oimx',
        'psen'
      ])

    dictionary = ~w(ananas roi orge remisa coquelicot plante axe)
    assert grid |> WordSearch.list_all(dictionary) |> Enum.sort() == ~w(axe orge remisa roi)
  end

  test "can list all words using a Stream to limit memory usage" do
    grid =
      Grid.new([
        'igre',
        'resa',
        'oimx',
        'psen'
      ])

    assert grid
           |> WordSearch.list_all(Language.dictionary("fr") |> Enum.into([]))
           |> Enum.sort() ==
             grid |> WordSearch.list_all(Language.dictionary("fr")) |> Enum.sort()
  end

  test "can find path of a word in a grid" do
    grid =
      Grid.new([
        'igre',
        'resa',
        'oimx',
        'psun'
      ])

    assert grid |> WordSearch.find_path_for("orge") == [{0, 2}, {0, 1}, {1, 0}, {1, 1}]

    assert grid |> WordSearch.find_path_for("misere") == [
             {2, 2},
             {1, 2},
             {2, 1},
             {1, 1},
             {2, 0},
             {3, 0}
           ]

    assert grid |> WordSearch.find_path_for("umxn") == [{2, 3}, {2, 2}, {3, 2}, {3, 3}]
    assert grid |> WordSearch.find_path_for("papillon") == []
  end
end
