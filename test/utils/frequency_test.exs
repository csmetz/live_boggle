defmodule Utils.FrequencyTest do
  use ExUnit.Case

  test "can compute bigrams frequency from dictionary" do
    assert Language.dictionary("fr")
           |> Utils.Frequency.bigram_frequency()
           |> get_in('bz') >= 0
  end

  test "can print bigrams" do
    freq =
      Language.dictionary("fr")
      |> Utils.Frequency.bigram_frequency()

    assert freq
           |> Utils.Frequency.print_bigrams()
           |> Utils.Frequency.read_bigrams() ==
             freq
  end

  # Was used to compute bigrams frequency, not a real test

  # test "dump" do
  #   content =
  #     Language.dictionary("en")
  #     |> Utils.Frequency.bigram_frequency()
  #     |> Utils.Frequency.print_bigrams()

  #   File.write!("bigrams.txt", content)
  # end
end
