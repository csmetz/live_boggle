defmodule Utils.RandomTest do
  use ExUnit.Case, async: true

  test "with a single value, always returns the same value" do
    assert Utils.Random.weighted_choice(%{"bim" => 1.0}) == "bim"
    assert Utils.Random.weighted_choice(%{:plop => 1.0}) == :plop
  end

  test "works with several values" do
    assert Utils.Random.weighted_choice(%{"a" => 0.0, "b" => 0.5, "c" => 0.5}) in ["b", "c"]
  end
end
